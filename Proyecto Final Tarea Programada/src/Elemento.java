import java.io.Serializable;

/**
 * @author Susana y Daniel
 * Programa que permite crear el objeto elemento 
 */
public class Elemento implements Serializable {
	private String nombre;
	private String formulaMolecular;
	private double pesoMolecular;
	private double densidad;
	private double indiceDeRefraccion;
	private String toxicidad;
	private int numeroAtomico;
	private double puntoDeEbullicion;
	private double puntoDeFusion;

	/**
	 * Este es el constructor por omisi�n de la clase. Requiere: N/A Efect�a:
	 * Establece los valores por defecto de la variables de la clase. Modifica:
	 * El valor de las variables se modifica a las establecidas.
	 */
	public Elemento() {
		nombre = "";
		formulaMolecular = "";
		pesoMolecular = 0;
		densidad = 0;
		indiceDeRefraccion = 0;
		toxicidad = "";
		numeroAtomico = 0;
		puntoDeEbullicion = 0;
		puntoDeFusion = 0;

	}

	/**
	 * Establece el valor de las variables al crear el objeto. Requiere: Que las
	 * variables est�n correctamente inicializadas. Efectu�: Establece los
	 * valores de las variables. Modifica: Los valores de las variables a los
	 * valores de los par�metros recibidos .
	 * 
	 * @param nombre
	 * @param formulaMolecular
	 * @param pesoMolecular
	 * @param densidad
	 * @param indiceDeRefraccion
	 * @param toxicidad
	 * @param numeroAtomico
	 * @param puntoDeEbullicion
	 * @param puntoDeFusion
	 */
	public Elemento(String nombre, String formulaMolecular, double pesoMolecular, double densidad,
			double indiceDeRefraccion, String toxicidad, int numeroAtomico, double puntoDeEbullicion,
			double puntoDeFusion) {

		this.nombre = nombre;
		this.formulaMolecular = formulaMolecular;
		this.pesoMolecular = pesoMolecular;
		this.densidad = densidad;
		this.indiceDeRefraccion = indiceDeRefraccion;
		this.toxicidad = toxicidad;
		this.numeroAtomico = numeroAtomico;
		this.puntoDeEbullicion = puntoDeEbullicion;
		this.puntoDeFusion = puntoDeFusion;
	}

	/**
	 * Devuelve el valor del �nombre�. Requiere: Que la variable �nombre� est�
	 * correctamente inicializada. Efect�a: Retorna el valor de la variable
	 * �nombre�. Modifica: N/A
	 * 
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Asigna el valor recibido por par�metro a "nombre". Requiere: N/A Efect�a:
	 * Establece el valor de la variable "nombre". Modifica: El valor de
	 * "nombre" se modifica al valor de la variable �nombreAsignado�.
	 * 
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Devuelve el valor de la f�rmulaMolecular. Requiere: Que la variable
	 * f�rmulaMolecular est� correctamente inicializada. Efect�a: Retorna el
	 * valor de la variable �f�rmulaMolecular�. Modifica: N/A
	 * 
	 * @return the formulaMolecular
	 */
	public String getFormulaMolecular() {
		return formulaMolecular;
	}

	/**
	 * Asigna el valor recibido por par�metro a "f�rmulaMolecular". Requiere:
	 * N/A Efect�a: Establece el valor de la variable "f�rmulaMolecular".
	 * Modifica: El valor de "f�rmulaMolecular" se modifica al valor de la
	 * variable �formulaAsignada�.
	 * 
	 * @param formula
	 *            the formula to set
	 */
	public void setFormulaMolecular(String formulaMolecular) {
		this.formulaMolecular = formulaMolecular;
	}

	/**
	 * Devuelve el valor del peso molecular. Requiere: Que la variable
	 * pesoMolecular est� correctamente inicializada. Efect�a: Retorna el valor
	 * de la variable �pesoMolecular�. Modifica: N/A
	 * 
	 * @return the pesoMolecular
	 */
	public double getPesoMolecular() {
		return pesoMolecular;
	}

	/**
	 * Asigna el valor recibido por par�metro a "pesoMolecular". Requiere: N/A
	 * Efect�a: Establece el valor de la variable "pesoMolecular". Modifica: El
	 * valor de "pesoMolecular" se modifica al valor de la variable
	 * �pesoAsignado�.
	 * 
	 * @param pesoMolecular
	 *            the pesoMolecular to set
	 */
	public void setPesoMolecular(double pesoMolecular) {
		this.pesoMolecular = pesoMolecular;
	}

	/**
	 * Devuelve el valor de la densidad. Requiere: Que la variable densidad est�
	 * correctamente inicializada. Efect�a: Retorna el valor de la variable
	 * �densidad�. Modifica: N/A
	 * 
	 * @return the densidad
	 */
	public double getDensidad() {
		return densidad;
	}

	/**
	 * Asigna el valor recibido por par�metro a "densidad". Requiere: N/A
	 * Efect�a: Establece el valor de la variable "densidad". Modifica: El valor
	 * de "densidad" se modifica al valor de la variable �densidadAsignada�.
	 * 
	 * @param densidad
	 *            the densidad to set
	 */
	public void setDensidad(double densidad) {
		this.densidad = densidad;
	}

	/**
	 * Devuelve el valor del �ndice de refracci�n. Requiere: Que la variable
	 * indiceDeRefraccion est� correctamente inicializada. Efect�a: Retorna el
	 * valor de la variable �indiceDeRefraccion�. Modifica: N/A
	 * 
	 * @return the indiceDeRefraccion
	 */
	public double getIndiceDeRefraccion() {
		return indiceDeRefraccion;
	}

	/**
	 * Asigna el valor recibido por par�metro a "indiceDeRefraccion". Requiere:
	 * N/A Efect�a: Establece el valor de la variable "indiceDeRefraccion".
	 * Modifica: El valor de "indiceDeRefraccion" se modifica al valor de la
	 * variable �indiceAsignado�.
	 * 
	 * @param indiceDeRefraccion
	 *            the indiceDeRefraccion to set
	 */
	public void setIndiceDeRefraccion(double indiceDeRefraccion) {
		this.indiceDeRefraccion = indiceDeRefraccion;
	}

	/**
	 * Devuelve una cadena que contiene la informaci�n de �toxicidad�. Requiere:
	 * Que la variable �toxicidad� est� correctamente inicializada. Efect�a:
	 * Retorna el valor de la variable �toxicidad�. Modifica: N/A
	 * 
	 * @return the toxicidad
	 */
	public String getToxicidad() {
		return toxicidad;
	}

	/**
	 * Asigna el valor recibido por par�metro a "toxicidad". Requiere: N/A
	 * Efect�a: Establece el valor de la variable "toxicidad". Modifica: El
	 * valor de "toxicidad" se modifica al valor de la variable
	 * �toxicidadAsignada�.
	 * 
	 * @param toxicidad
	 *            the toxicidad to set
	 */
	public void setToxicidad(String toxicidad) {
		this.toxicidad = toxicidad;
	}

	/**
	 * Devuelve el valor del �numeroAtomico�. Requiere: Que la variable
	 * �numeroAtomico� est� correctamente inicializada. Efect�a: Retorna el
	 * valor de la variable �numeroAtomico�. Modifica: N/A
	 * 
	 * @return the numeroAtomico
	 */
	public int getNumeroAtomico() {
		return numeroAtomico;
	}

	/**
	 * Asigna el valor recibido por par�metro a "numeroAtomico". Requiere: N/A
	 * Efect�a: Establece el valor de la variable "numeroAtomico". Modifica: El
	 * valor de "numeroAtomico" se modifica al valor de la variable
	 * �numeroAsignado�.
	 * 
	 * @param numeroAtomico
	 *            the numeroAtomico to set
	 */
	public void setNumeroAtomico(int numeroAtomico) {
		this.numeroAtomico = numeroAtomico;
	}

	/**
	 * Devuelve el valor del punto de ebullici�n. Requiere: Que la
	 * variable�puntoDeEbullcion� est� correctamente inicializada. Efect�a:
	 * Retorna el valor de la variable �puntoDeEbullcion�. Modifica: N/A
	 * 
	 * @return the puntoDeEbullicion
	 */
	public double getPuntoDeEbullicion() {
		return puntoDeEbullicion;
	}

	/**
	 * Asigna el valor recibido por par�metro a "puntoDeEbullcion". Requiere:
	 * N/A Efect�a: Establece el valor de la variable "puntoDeEbullcion".
	 * Modifica: El valor de "puntoDeEbullcion" se modifica al valor de la
	 * variable �ebullici�nAsignada�.
	 * 
	 * @param puntoDeEbullicion
	 *            the puntoDeEbullicion to set
	 */
	public void setPuntoDeEbullicion(double puntoDeEbullicion) {
		this.puntoDeEbullicion = puntoDeEbullicion;
	}

	/**
	 * Devuelve el valor del punto de fusi�n. Requiere: Que la variable
	 * �puntoDeFusion�est� correctamente inicializada. Efect�a: Retorna el valor
	 * de la variable �puntoDeFusion�. Modifica: N/A
	 * 
	 * @return the puntoDeFusion
	 */
	public double getPuntoDeFusion() {
		return puntoDeFusion;
	}

	/**
	 * Asigna el valor recibido por par�metro a "puntoDeFusion". Requiere: N/A
	 * Efect�a: Establece el valor de la variable "puntoDeFusion". Modifica: El
	 * valor de "puntoDeFusion" se modifica al valor de la variable
	 * �fusionAsignada�.
	 * 
	 * @param puntoDeFusion
	 *            the puntoDeFusion to set
	 */
	public void setPuntoDeFusion(double puntoDeFusion) {
		this.puntoDeFusion = puntoDeFusion;
	}
	
	@Override
	public String toString() {
		return "-" + "El nombre es: " + (this.nombre) +"\n" + 
				"-" + "La f�rmula molecular es: " + (this.formulaMolecular) +"\n" + 
				"-" + "El peso molecular en g/mol es: " + (this.pesoMolecular) +"\n" + 
				"-" + "El �ndice de refracci�n es: " + (this.indiceDeRefraccion) +"\n" + 
				"-" + "La densidad en kg/m3 es: " + (this.densidad) + "\n" +
				"-" + "La toxicidad es: " + (this.toxicidad) +"\n" + 
				"-" + "El n�mero at�mico es: " + (this.numeroAtomico) +"\n" + 
				"-" + "El punto de ebullici�n en �C es: " + (this.puntoDeEbullicion) +"\n" + 
				"-" + "El punto de fusi�n en �C es: " + (this.puntoDeFusion) +"\n";
	}

}
