import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

/**
 * @author Susana Sol�rzano J y Daniel Matamoros A Program que contiene los
 *         m�todos que permiten la edici�n de lod vectores que contengan objetos
 *         tipo elemento y compuesto
 *
 */
public class Utilitarios {
	// Varaibles que son necesarios para el funcionamiento fe los m�todos
	private Elemento[] vectorDeElementos;
	private Compuesto[] vectorDeCompuestos;

	/**
	 * M�todo Constructor Requiere:N/A Modifica:N/A Efectua: da valores
	 * predeterminados a los compuestos y elementos de los vectores
	 * correspodientes
	 */
	public Utilitarios() {
		vectorDeElementos = new Elemento[10];
		vectorDeElementos[0] = new Elemento("Hidr�geno", "H", 1.00794, 0.0899, 1.000132,
				"La principal precuaci�n a tomar con el hidr�geno es el hecho de que es muy inflamable", 1, -253, -259);

		vectorDeElementos[1] = new Elemento("Carbono", "C", 12.0111, 2267, 0,
				"En general el carbono s�lido no es t�xico al ser humano, sin embargo, el polvo de carb�n puede irritar �rganos del sistema respiratorio al inhalar. \n Este polvo usualmente viene de los volcanes.",
				6, 4826.85, 3526.85);

		vectorDeElementos[2] = new Elemento("Ox�geno", "O", 15.999, 1.429, 1.2243,
				"El ox�geno solo presenta toxicidad a presiones parciales elevadas.", 8, -183, -223);

		vectorDeElementos[3] = new Elemento("Kript�n", "Kr", 83.798, 3.708, 1.303,
				"Su peligrosidad est� en la inhalaci�n ya que puede asfixiar y causar mareos, nauseas y en algunos casos la muerte",
				36, -152, -157.3);

		vectorDeElementos[4] = new Elemento("Cloro", "Cl", 35.453, 3.214, 1.383,
				"Irrita la piel, los ojos y el sistema respiratorio", 17, -34, -102);

		vectorDeElementos[5] = new Elemento("Litio", "Li", 6.941, 535, 0,
				"El litio reacciona violentamente con ciertas sustancias como el agua. Puede ocasionar sensaci�n de quemadura y dolores abdominales al inhalar o inherir",
				3, 1330, 180.5);

		vectorDeElementos[6] = new Elemento("Uranio", "U", 238.03, 19070, 0,
				"El uranio es radioactivo, lo que representa un gran riesgo para la salud en exposiciones elevadas. \n La exposici�n a la radiaci�n continua puede llegar a causar mutaciones en los sistemas del ser humano lo que puede llevar a provocar c�ncer.",
				92, 3818, 1132);

		vectorDeElementos[7] = new Elemento("Germanio", "Ge", 72.59, 5320, 4.01,
				"El metal pesado principalmente puede afectar a sistemas acu�ticos.", 32, 2830, 937.4);

		vectorDeElementos[8] = new Elemento("Yodo", "I", 126.904, 4940, 3.34,
				"Las principales amenazas del yodo se encuentran en sus is�topos radioactivos.", 53, 183, 113.7);

		vectorDeElementos[9] = new Elemento("Azufre", "S", 32.064, 2070, 1.917,
				"La principales amenazas contra la salud son irritaciones en la piel y en el sistema respiratorio, des�rdenes en el sistema gastrointestinal y da�os card�acos.",
				16, 444.6, 119.0);

		vectorDeCompuestos = new Compuesto[10];
		vectorDeCompuestos[0] = new Compuesto("�cido fluoroantim�nico", "HSbF6", 236.808, 0, 0,
				"ALtamente reactivo y corrosivo, adem�s de reaccionar de forma explosiva", 0, 0);
		vectorDeCompuestos[1] = new Compuesto("�cido ac�tico", "CH3COOH", 60.02, 1.049, 1372,
				"Puede dgenerar irritaci�n al contacto y dolor abdominal en caso de ingesti�n", 118, 17);
		vectorDeCompuestos[2] = new Compuesto("Cloruro de plata ", "AgCl", 143.32, 5560, 2.0668,
				"Pueded causar irrtaci�n al contacto, adem�s de ser t�xico para ecosistemas acu�ticos", 1547, 457);
		vectorDeCompuestos[3] = new Compuesto("Cloruro de sodio", "NaCl", 58.4, 2160, 1.544,
				"Puede dgenerar irritaci�n al contacto y dolor abdominal en caso de ingesti�n", 1465, 801);
		vectorDeCompuestos[4] = new Compuesto("Cuarzo", "SiO2", 60.08, 2634, 1.544,
				"Puede dgenerar irritaci�n al contacto y dolor abdominal en caso de ingesti�n", 2230, 1713);
		vectorDeCompuestos[5] = new Compuesto("Acetona", "C3H6O", 58.08, 791, 1.359,
				"T�xico en caso de ingesti�n y peligroso al contacto de piel y ojos", 56, -95);
		vectorDeCompuestos[6] = new Compuesto("�cido sulf�rico", "H2SO4", 98.08, 1800, 1.4183, "ALtamente corrosivo",
				337, 10);
		vectorDeCompuestos[7] = new Compuesto("Etanol", "CH3CH2OH", 46.07, 789, 1.361, "Copuesto altamente vol�til", 78,
				-114);
		vectorDeCompuestos[8] = new Compuesto("Cloroformo", "CHCl3", 119.38, 1483, 1.48,
				"Los productos de la oxidaci�n de este producto son t�sicos y corrosivos", 61, -64);
		vectorDeCompuestos[9] = new Compuesto("Agua", "H2O", 18.02, 1000, 1.333, "No presenta", 99.98, 0);
	}

	/**
	 * M�todo que guarda la informaci�n del vector en un archivo Req: El uso de
	 * clases predeterminadas en Java Efect: Guarda los archivos que el ususario
	 * quiera Mod: N/A
	 */
	public void guardarElementos() {
		File miFile;
		ObjectOutputStream variableOutput;
		miFile = new File("Elementos");
		try {
			variableOutput = new ObjectOutputStream(new FileOutputStream(miFile));
			variableOutput.writeObject(vectorDeElementos);
			variableOutput.close();
		} catch (IOException excepcionIO) {
		}

	}
	
	/**
	 * M�todo que carga la informaci�n del vector en un archivo Req: El uso de
	 * clases predeterminadas en Java Efect: Carga los archivos que el ususario
	 * quiera Mod: N/A
	 */
	public void cargarElementos() {
		ObjectInputStream variableInput;
		File miFile;
		miFile = new File("Elementos");
		try {
			variableInput = new ObjectInputStream(new FileInputStream(miFile));
			vectorDeElementos = (Elemento[]) variableInput.readObject();
		} catch (ClassNotFoundException excepcionClaseNoEncontrada) {
			Logger.getLogger(Utilitarios.class.getName()).log(Level.SEVERE, null, excepcionClaseNoEncontrada);
		} catch (IOException excepcionIO) {
		} catch (ArrayIndexOutOfBoundsException excepcionFueraLimite) {
			JOptionPane.showMessageDialog(null, excepcionFueraLimite.getMessage());
		}
	}

	/**
	 * M�todo que guarda la informaci�n del vector en un archivo Req: El uso de
	 * clases predeterminadas en Java Efect: Guarda los archivos que el ususario
	 * quiera Mod: N/A
	 */
	public void guardarCompuestos() {
		File miFile;
		ObjectOutputStream variableOutput;

		miFile = new File("Compuestos");

		try {
			variableOutput = new ObjectOutputStream(new FileOutputStream(miFile));
			variableOutput.writeObject(vectorDeCompuestos);
			variableOutput.close();
		} catch (IOException excepcionIO) {
		}

		
	}
	
	/**
	 * M�todo que carga la informaci�n del vector en un archivo Req: El uso de
	 * clases predeterminadas en Java Efect: Carga los archivos que el ususario
	 * quiera Mod: N/A
	 */
	public void cargarCompuestos() {
		ObjectInputStream variableInput;
		File miFile;
		miFile = new File("Compuestos");
		
		try {
			variableInput = new ObjectInputStream(new FileInputStream(miFile));
			vectorDeCompuestos = (Compuesto[]) variableInput.readObject();
		} catch (ClassNotFoundException excepcionClaseNoEncontrada) {
			Logger.getLogger(Utilitarios.class.getName()).log(Level.SEVERE, null, excepcionClaseNoEncontrada);
		} catch (IOException excepcionIO) {
		} catch (ArrayIndexOutOfBoundsException excepcionFueraLimite) {
			JOptionPane.showMessageDialog(null, excepcionFueraLimite.getMessage());
		}

	}

	/**
	 * M�todo que permite listar todos los elementos que contenga el programa
	 * Req: Variables inicializadas Efect: Imprime la lista de elementos Mod:
	 * N/A
	 * 
	 * @return impresion
	 */
	public String listarElementos() {
		String impresion = "";
		for (int indice = 0; indice < vectorDeElementos.length; indice++) {
			impresion += " - " + vectorDeElementos[indice].getNombre();
			impresion += "\n";
		}

		return impresion;
	}

	/**
	 * M�todo que permite listar todos los compuestos que contenga el programa
	 * Req: Variables inicializadas Efect: Imprime la lista de compuestos Mod:
	 * N/A
	 * 
	 * @return impresion
	 */
	public String listarCompuestos() {
		String impresion = "";
		for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
			impresion += " - " + vectorDeCompuestos[indice].getNombre();
			impresion += "\n";
		}

		return impresion;
	}

	/**
	 * M�todo que devuelve todas las caracer�sticas de un elemento Requiere:
	 * Variables inicializadas Efect�a: Le ense�a al usuario las caracter�sticas
	 * de un elemento Mod: N/A
	 * 
	 * @param nombre
	 * @return vectorDeElementos[indice]
	 */
	public void ense�arCaracteristicasDeElemento(String nombre) {
		int variableAyudante = 0;
		if (vectorDeElementos == null) {
			JOptionPane.showMessageDialog(null, "No hay informaci�n para mostrar");

		}
		if (!busquedaSimpleElementos(nombre)) {
			JOptionPane.showMessageDialog(null, "No existen elementos con ese nombre en los datos guardados");
		} else {
			for (int indice = 0; indice < vectorDeElementos.length; indice++) {
				if (vectorDeElementos[indice].getNombre().equalsIgnoreCase(nombre)) {
					variableAyudante = indice;
					JOptionPane.showMessageDialog(null, vectorDeElementos[variableAyudante]);
				}

			}

		}

	}

	/**
	 * M�todo que devuelve todas las caracer�sticas de un compuesto Requiere:
	 * Variables inicializadas Efect�a: Le ense�a al usuario las caracter�sticas
	 * de un compuesto Mod: N/A
	 * 
	 * @param nombre
	 * @return vectorDeCompuestos[indice]
	 */
	public void ense�arCaracteristicasDeCompuesto(String nombre) {
		int variableAyudante = 0;
		if (vectorDeCompuestos == null) {
			JOptionPane.showMessageDialog(null, "No hay informaci�n para mostrar");
		}
		if (!busquedaSimpleCompuestos(nombre)) {
			JOptionPane.showMessageDialog(null, "No existen compuestos con ese nombre en los datos guardados");
		} else {
			for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
				if (vectorDeCompuestos[indice].getNombre().equalsIgnoreCase(nombre)) {
					variableAyudante = indice;
					JOptionPane.showMessageDialog(null, vectorDeCompuestos[variableAyudante]);
				}
			}

		}

	}

	/**
	 * Requiere: que la variable nombre se encuentre correctamente inicializade.
	 * Modifica: los compuestos que almacena el vectorDeCompuestos Efectua:
	 * elimina un compuesto del vector dependiendo del nombre que ingresa el
	 * ususario
	 * 
	 * @param nombre:
	 *            variable tipo String que se refiere al compuesto que se desea
	 *            eliminar
	 * @return valor booleano que indica si se pudo iniciar correctamente o no
	 */

	public boolean borrarCompuesto(String nombre) {
		// variable de retorno
		boolean borrado = false;
		// variable que busca la posici�n en la que se encuentra el compuesto
		// que se desea eliminar
		int posicion = 0;
		// condici�n para comprobar que sea un vector nulo
		if (vectorDeCompuestos == null) {
			// booleano falso ya que no se puede eliminar nada
			borrado = false;
			// condici�n contraria
		} else {
			if (busquedaSimpleCompuestos(nombre)) {
				// inicia la variable temporal de un vector m�s peque�o
				Compuesto[] vectorTemporal;
				vectorTemporal = new Compuesto[vectorDeCompuestos.length - 1];
				// ciclo que permite encontrar la posici�n es la que se
				// encuentra el compuesto que se desea eliminar
				for (int indice = 0; !(vectorDeCompuestos[indice].getNombre().equalsIgnoreCase(nombre)); indice++) {
					// guarada el valor num�rico de la celda en la que se
					// encuentra el compuesto a eliminar
					posicion = indice + 1;
				}
				// Cambio de posici�n de la persona que se desea eliminar con la
				// �ltima posici�n del vector
				Compuesto compuestoTemporal = vectorDeCompuestos[posicion];
				vectorDeCompuestos[posicion] = vectorDeCompuestos[vectorDeCompuestos.length - 1];
				vectorDeCompuestos[vectorDeCompuestos.length - 1] = compuestoTemporal;
				// permite copiar todos los compuestos con excepci�n del �ltimo
				for (int indice = 0; indice < vectorTemporal.length; indice++) {
					vectorTemporal[indice] = vectorDeCompuestos[indice];
				}
				// asocia el vector de compuestos igual a la informaci�n que
				// contenga el vector temporal
				vectorDeCompuestos = vectorTemporal;
				// valor booleano cierto, ya que se elimina satisfactoriamente
				borrado = true;
			}
		}
		return borrado;
	}

	/**
	 * Requiere: que la variable nombre est� correctamente inicializada
	 * Modifica: los elementos que se encuentran en el vector Efectua: elimina
	 * el elemento de nombre igual ingresado por el usuario
	 * 
	 * @param nombre
	 *            : vaiable tipo String que corresponde al nombre del elemento
	 *            que se desea eliminar
	 * @return : un valor booleano que ser� afirmativo se se logr� eliminar y
	 *         negativo en caso contrario
	 */

	public boolean borrarElemento(String nombre) {
		// variable de retorno
		boolean borrado = false;
		// variable que busca la posici�n en la que se encuentra el compuesto
		// que se desea eliminar
		int posicion = 0;
		// condici�n para comprobar que sea un vector nulo
		if (vectorDeElementos == null) {
			// booleano falso ya que no se puede eliminar nada
			borrado = false;
			// condici�n contraria
		} else {
			if (busquedaSimpleElementos(nombre)) {
				// inicia la variable temporal de un vector m�s peque�o
				Elemento[] vectorTemporal;
				vectorTemporal = new Elemento[vectorDeElementos.length - 1];
				// ciclo que permite encontrar la posici�n es la que se
				// encuentra el compuesto que se desea eliminar
				for (int indice = 0; !(vectorDeElementos[indice].getNombre().equalsIgnoreCase(nombre)); indice++) {
					// guarada el valor num�rico de la celda en la que se
					// encuentra el compuesto a eliminar
					posicion = indice + 1;
				}
				// Cambio de posici�n del elemento que se desea eliminar con la
				// �ltima posici�n del vector
				Elemento elementoTemporal = vectorDeElementos[posicion];
				vectorDeElementos[posicion] = vectorDeElementos[vectorDeElementos.length - 1];
				vectorDeElementos[vectorDeElementos.length - 1] = elementoTemporal;
				// permite copiar todos los compuestos con excepci�n del �ltimo
				for (int indice = 0; indice < vectorTemporal.length; indice++) {
					vectorTemporal[indice] = vectorDeElementos[indice];
				}
				// asocia el vector de elementos igual a la informaci�n que
				// contenga el vector temporal
				vectorDeElementos = vectorTemporal;
				// valor booleano cierto, ya que se elimina satisfactoriamente
				borrado = true;
			}
		}
		return borrado;
	}

	/**
	 * Requiere: N/A Modifica: el orden en el que se encuentran los compuestos
	 * Efectua: ordena alfab�ticamente los compuestos que posea el vector
	 * 
	 * @return: valor booleano que identifica si se pudo ordenar correctamente
	 */
	public boolean ordenarAlfab�ticamenteCompuesto() {
		// Variable booleana para retorno
		boolean terminado = false;
		// En caso de que sea nulo
		if (vectorDeCompuestos == null) {
			// asociaci�n del valor de la retorno
			terminado = false;
			// caso contrario
		} else {
			// ciclo para compara y ordenar los valores
			for (int indice = 0; indice < vectorDeCompuestos.length - 1; indice++) {
				for (int subindice = 0; subindice < vectorDeCompuestos.length - 1; subindice++) {
					String cadena = vectorDeCompuestos[subindice].getNombre();
					String cadenaSiguiente = vectorDeCompuestos[subindice + 1].getNombre();
					// Comparaci�n entre los valores
					if (cadena.compareToIgnoreCase(cadenaSiguiente) > 0) {
						// si est�n desordenados esto cambia de posici�n
						Compuesto compuestoTemperal = new Compuesto();
						compuestoTemperal = vectorDeCompuestos[subindice];
						vectorDeCompuestos[subindice] = vectorDeCompuestos[subindice + 1];
						vectorDeCompuestos[subindice + 1] = compuestoTemperal;
					}
				}
			}
			// asignaci�n de la variable booleana
			terminado = true;
		}
		// comando de retorna
		return terminado;
	}

	/**
	 * Requiere: N/A Modifica: el orden en el que se encuentran los elemnetos
	 * Efectua: ordena alfab�ticamente los elementos dentro del vector @ return:
	 * valor booleano en caso de que se puedan ordenar correctamente retorna
	 * cierto, en caso contrario falso
	 */
	public boolean ordenarAlfab�ticamenteElemento() {
		// Variable booleana para retorno
		boolean terminado = false;
		// En caso de que sea nulo
		if (vectorDeElementos == null) {
			// asociaci�n del valor de la retorno
			terminado = false;
			// caso contrario
		} else {
			// ciclo para compara y ordenar los valores
			for (int indice = 0; indice < vectorDeElementos.length - 1; indice++) {
				for (int subindice = 0; subindice < vectorDeElementos.length - 1; subindice++) {
					String cadena = vectorDeElementos[subindice].getNombre();
					String cadenaSiguiente = vectorDeElementos[subindice + 1].getNombre();
					// Comparaci�n entre los valores
					if (cadena.compareToIgnoreCase(cadenaSiguiente) > 0) {
						// si est�n desordenados esto cambia de posici�n
						Elemento elementoTemperal = new Elemento();
						elementoTemperal = vectorDeElementos[subindice];
						vectorDeElementos[subindice] = vectorDeElementos[subindice + 1];
						vectorDeElementos[subindice + 1] = elementoTemperal;
					}
				}
			}
			// asignaci�n de la variable booleana
			terminado = true;
		}
		// comando de retorna
		return terminado;
	}

	/**
	 * M�todo para agregar la informaci�n de un nuevo elemento Req: Variables
	 * inicializadas Efect: Agrega un nuevo elemento al vector Mod: La
	 * informaci�n del vector de elementos
	 * 
	 * @param nombre
	 * @param formulaMolecular
	 * @param pesoMolecular
	 * @param densidad
	 * @param indiceDeRefraccion
	 * @param toxicidad
	 * @param numeroAtomico
	 * @param puntoDeEbullicion
	 * @param puntoDeFusion
	 * @return true o false
	 */
	public boolean agregarElemento(String nombre, String formulaMolecular, double pesoMolecular, double densidad,
			double indiceDeRefraccion, String toxicidad, int numeroAtomico, double puntoDeEbullicion,
			double puntoDeFusion) {

		for (int indice = 0; indice < vectorDeElementos.length; indice++) {
			if (vectorDeElementos[indice] == null) {

				vectorDeElementos = new Elemento[1];
				vectorDeElementos[0] = new Elemento(nombre, formulaMolecular, pesoMolecular, densidad,
						indiceDeRefraccion, toxicidad, numeroAtomico, puntoDeEbullicion, puntoDeFusion);
				return true;
			}

		}
		if (!busquedaSimpleElementos(nombre)) {

			Elemento vectorTemporal[];
			vectorTemporal = new Elemento[vectorDeElementos.length + 1];
			for (int indiceDos = 0; indiceDos < vectorDeElementos.length; indiceDos++) {

				vectorTemporal[indiceDos] = vectorDeElementos[indiceDos];

			}

			vectorTemporal[vectorDeElementos.length] = new Elemento(nombre, formulaMolecular, pesoMolecular, densidad,
					indiceDeRefraccion, toxicidad, numeroAtomico, puntoDeEbullicion, puntoDeFusion);

			vectorDeElementos = vectorTemporal;
			return true;
		}

		return false;

	}

	/**
	 * M�todo para agregar la informaci�n de un nuevo compuesto Req: Variables
	 * inicializadas Efect: Agrega un nuevo compuesto al vector Mod: La
	 * informaci�n del vector de compuestos
	 * 
	 * @param nombre
	 * @param formulaMolecular
	 * @param pesoMolecular
	 * @param densidad
	 * @param indiceDeRefraccion
	 * @param toxicidad
	 * @param puntoDeEbullicion
	 * @param puntoDeFusion
	 * @return true o false
	 */
	public boolean agregarCompuesto(String nombre, String formulaMolecular, double pesoMolecular, double densidad,
			double indiceDeRefraccion, String toxicidad, double puntoDeEbullicion, double puntoDeFusion) {

		for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
			if (vectorDeCompuestos[indice] == null) {

				vectorDeCompuestos = new Compuesto[1];
				vectorDeCompuestos[0] = new Compuesto(nombre, formulaMolecular, pesoMolecular, densidad,
						indiceDeRefraccion, toxicidad, puntoDeEbullicion, puntoDeFusion);
				return true;
			}

		}
		if (!busquedaSimpleCompuestos(nombre)) {

			Compuesto vectorTemporal[];
			vectorTemporal = new Compuesto[vectorDeCompuestos.length + 1];
			for (int indiceDos = 0; indiceDos < vectorDeCompuestos.length; indiceDos++) {

				vectorTemporal[indiceDos] = vectorDeCompuestos[indiceDos];

			}

			vectorTemporal[vectorDeCompuestos.length] = new Compuesto(nombre, formulaMolecular, pesoMolecular, densidad,
					indiceDeRefraccion, toxicidad, puntoDeEbullicion, puntoDeFusion);

			vectorDeCompuestos = vectorTemporal;
			return true;
		}

		return false;

	}

	/**
	 * M�todo que permite editar la informaci�n de un elemento Req: Variables
	 * Inicializadas Efect: Edita la informaci�n de uno de los elementos Mod: La
	 * informaci�n de uno de los elementos
	 * 
	 * @param nombre
	 * @param formulaMolecular
	 * @param pesoMolecular
	 * @param densidad
	 * @param indiceDeRefraccion
	 * @param toxicidad
	 * @param numeroAtomico
	 * @param puntoDeEbullicion
	 * @param puntoDeFusion
	 * @return true o false
	 */
	public boolean editarElemento(String nombre, String formulaMolecular, double pesoMolecular, double densidad,
			double indiceDeRefraccion, String toxicidad, int numeroAtomico, double puntoDeEbullicion,
			double puntoDeFusion) {

		int variableInt = 0;

		for (int indice = 0; indice < vectorDeElementos.length; indice++) {
			if (vectorDeElementos[indice] == null) {
				return false;
			}
		}

		for (int indice = 0; indice < vectorDeElementos.length; indice++) {
			if (vectorDeElementos[indice].getNombre().equalsIgnoreCase(nombre)) {
				Elemento vectorTemporal[];
				vectorTemporal = new Elemento[vectorDeElementos.length];

				variableInt = indice;

				for (int indiceDos = 0; indiceDos < vectorDeElementos.length; indiceDos++) {
					vectorTemporal[indiceDos] = vectorDeElementos[indiceDos];
				}

				vectorTemporal[variableInt] = new Elemento(nombre, formulaMolecular, pesoMolecular, densidad,
						indiceDeRefraccion, toxicidad, numeroAtomico, puntoDeEbullicion, puntoDeFusion);

				vectorDeElementos = vectorTemporal;
				return true;

			}

		}

		return false;
	}

	/**
	 * M�todo que permite editar la informaci�n de un compuesto Req: Variables
	 * Inicializadas Efect: Edita la informaci�n de uno de los compuestos Mod:
	 * La informaci�n de uno de los compuestos
	 * 
	 * @param nombre
	 * @param formulaMolecular
	 * @param pesoMolecular
	 * @param densidad
	 * @param indiceDeRefraccion
	 * @param toxicidad
	 * @param puntoDeEbullicion
	 * @param puntoDeFusion
	 * @return true o false
	 */
	public boolean editarCompuesto(String nombre, String formulaMolecular, double pesoMolecular, double densidad,
			double indiceDeRefraccion, String toxicidad, double puntoDeEbullicion, double puntoDeFusion) {

		int variableInt = 0;

		for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
			if (vectorDeCompuestos[indice] == null) {
				return false;
			}
		}

		for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
			if (vectorDeCompuestos[indice].getNombre().equalsIgnoreCase(nombre)) {
				Compuesto vectorTemporal[];
				vectorTemporal = new Compuesto[vectorDeCompuestos.length];

				variableInt = indice;

				for (int indiceDos = 0; indiceDos < vectorDeCompuestos.length; indiceDos++) {
					vectorTemporal[indiceDos] = vectorDeCompuestos[indiceDos];
				}

				vectorTemporal[variableInt] = new Compuesto(nombre, formulaMolecular, pesoMolecular, densidad,
						indiceDeRefraccion, toxicidad, puntoDeEbullicion, puntoDeFusion);

				vectorDeCompuestos = vectorTemporal;
				return true;

			}

		}

		return false;
	}

	/**
	 * Requiere: que la variable nombre se encuentre correctamente inicializada
	 * Modififca: N/A Efctua: realiza una busqueda simple en el vector de
	 * compuestos
	 * 
	 * @param nombre:
	 *            variable tipo String que se relaciona con el nombre que posee
	 *            un compuesto
	 * @return existe: variable booleana que retorna "true" en caso de que se
	 *         encontrara la variable y "false" en caso contrario
	 */
	public boolean busquedaSimpleCompuestos(String nombre) {
		// variable booleana
		boolean existe = false;
		// ciclo para compararlos nombres con el nombre ingresado
		if (vectorDeCompuestos != null) {
			// ciclo opara poder buscar en todas las personas del vector
			for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
				// asociaci�n de nombreTemporal con el nombre del compuesto en
				// el
				// vector
				String nombreTemporal = vectorDeCompuestos[indice].getNombre();
				// Comparaci�n entre el nombre del vector y el ingrasdo
				if (nombreTemporal.equalsIgnoreCase(nombre)) {
					// Si se cumple asignar cierto a la variable booleana
					existe = true;
					// en el caso contrario
				}
			}
		}
		// condici�n de retorno
		return existe;
	}

	/**
	 * Modifica: N/A Requiere: que la variable de nombre se inicie
	 * corrrectamente Efectua: realiza una busqueda simple en el vector de
	 * elementos
	 * 
	 * @param nombre:
	 *            variable de tipo String que corresponde al nombre del elemento
	 *            que se desea buscar
	 * @return existe:variable booleana que retorna "true" en caso de que se
	 *         encontrara la variable y "false" en caso contrario
	 */
	public boolean busquedaSimpleElementos(String nombre) {
		// variable booleana
		boolean existe = false;
		// ciclo para compararlos nombres con el nombre ingresado
		if (vectorDeElementos != null) {
			// ciclo opara poder buscar en todas las personas del vector
			for (int indice = 0; indice < vectorDeElementos.length; indice++) {
				// asociaci�n de nombreTemporal con el nombre del elementos en
				// el
				// vector
				String nombreTemporal = vectorDeElementos[indice].getNombre();
				// Comparaci�n entre el nombre del vector y el ingrasdo
				if (nombreTemporal.equalsIgnoreCase(nombre)) {
					// Si se cumple asignar cierto a la variable booleana
					existe = true;
					// en el caso contrario
				}
			}
		}
		// condici�n de retorno
		return existe;
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param formulaMolecular
	 *            :valor ingresado para que se compare con los datos de los
	 *            elementos guardados
	 * @return vectorDeElementos[indice].getNombre(): nombre del elemento que
	 *         contiene la misma f�rmula molecular que la ingresada
	 */
	public String busquedaPorFormulaMolecularElemento(String formulaMolecular) {
	
		if (vectorDeElementos == null) {
			JOptionPane.showMessageDialog(null, "No hay informaci�n para mostrar");
		}
		 else {
			for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
				if (vectorDeElementos[indice].getFormulaMolecular().equalsIgnoreCase(formulaMolecular)) {
					return vectorDeElementos[indice].getNombre();
				}
			}

		}
		return "Ning�n elemento cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param pesoMolecular:
	 *            valor ingresado para que se compare con los datos de los
	 *            elementos guardados
	 * @return vectorDeElementos[indice].getNombre() : nombre del elemento que
	 *         contiene el mismo peso molecular que el ingresado.
	 */
	public String busquedaPorPesoMolecularElemento(double pesoMolecular) {
		if (vectorDeElementos != null) {
			for (int indice = 0; indice < vectorDeElementos.length; indice++) {
				if (vectorDeElementos[indice].getPesoMolecular() == pesoMolecular) {
					return vectorDeElementos[indice].getNombre();
				}
			}
		}
		return "Ning�n elemento cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param densidad:
	 *            valor ingresado para que se compare con los datos de los
	 *            elementos guardados
	 * @return vectorDeElementos[indice].getNombre(): nombre del elemento
	 *         correspondiente a lo ingresado
	 */
	public String busquedaPorDensidadElemento(double densidad) {
		if (vectorDeElementos != null) {
			for (int indice = 0; indice < vectorDeElementos.length; indice++) {
				if (vectorDeElementos[indice].getDensidad() == densidad) {
					return vectorDeElementos[indice].getNombre();
				}
			}
		}
		return "Ning�n elemento cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param indiceDeRefraccion:
	 *            valor ingresado para que se compare con los datos de los
	 *            elementos guardados
	 * @return vectorDeElementos[indice].getNombre(): nombre del elemento
	 *         correspndiente a lo ingresado
	 */
	public String busquedaPorIndiceDeRefraccionElemento(double indiceDeRefraccion) {
		if (vectorDeElementos != null) {
			for (int indice = 0; indice < vectorDeElementos.length; indice++) {
				if (vectorDeElementos[indice].getIndiceDeRefraccion() == indiceDeRefraccion) {
					return vectorDeElementos[indice].getNombre();
				}
			}
		}
		return "Ning�n elemento cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param toxicidad
	 *            : valor de toxicidad buscado
	 * @return vectorDeElementos[indice].getNombre(): nombre con el valor de
	 *         toxicidad correspondiente
	 */
	public String busquedaPorToxicidadElemento(String toxicidad) {
		if (vectorDeElementos != null) {
			for (int indice = 0; indice < vectorDeElementos.length; indice++) {
				if (vectorDeElementos[indice].getToxicidad().equalsIgnoreCase(toxicidad)) {
					return vectorDeElementos[indice].getNombre();
				}
			}
		}
		return "Ning�n elemento cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param numeroAtomico
	 *            : valor ingresado para que se compare con los datos de los
	 *            elementos guardados
	 * @return vectorDeElementos[indice].getNombre(): nombre de elemento
	 *         correspondiente a los datos ingresados
	 */
	public String busquedaPorNumeroAtomicoElemento(int numeroAtomico) {
		if (vectorDeElementos != null) {
			for (int indice = 0; indice < vectorDeElementos.length; indice++) {
				if (vectorDeElementos[indice].getNumeroAtomico() == (numeroAtomico)) {
					return vectorDeElementos[indice].getNombre();
				}
			}
		}
		return "Ning�n elemento cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param puntoDeEbullicion:
	 *            valor ingresado para que se compare con los datos de los
	 *            elementos guardados
	 * @return vectorDeElementos[indice].getNombre(): nombre del elemento
	 *         correspondiente al punto de ebullici�n ingresado
	 */
	public String busquedaPorPuntoDeEbullicionElemento(double puntoDeEbullicion) {
		if (vectorDeElementos != null) {
			for (int indice = 0; indice < vectorDeElementos.length; indice++) {
				if (vectorDeElementos[indice].getPuntoDeEbullicion() == puntoDeEbullicion) {
					return vectorDeElementos[indice].getNombre();
				}
			}
		}
		return "Ning�n elemento cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param puntoDeFusion
	 *            valor ingresado para que se compare con los datos de los
	 *            elementos guardados
	 * @return vectorDeElementos[indice].getNombre(): nombre que corresponde a
	 *         al punto de fusi�n ingresado
	 */
	public String busquedaPorPuntoDeFusionElemento(double puntoDeFusion) {
		if (vectorDeElementos != null) {
			for (int indice = 0; indice < vectorDeElementos.length; indice++) {
				if (vectorDeElementos[indice].getPuntoDeFusion() == puntoDeFusion) {
					return vectorDeElementos[indice].getNombre();
				}
			}
		}
		return "Ning�n elemento cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param formulaMolecular:
	 *            corresponde a la f�rmula molecular de un compuesto
	 * @return vectorDeCompuestos[indice].getNombre(): nombre del compuesto que
	 *         se busca
	 */
	public String busquedaPorFormulaMolecularCompuesto(String formulaMolecular) {
		if (vectorDeCompuestos != null) {
			for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
				if (vectorDeCompuestos[indice].getFormulaMolecular().equalsIgnoreCase(formulaMolecular)) {
					return vectorDeCompuestos[indice].getNombre();
				}
			}
		}
		return "Ning�n compuesto cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param pesoMolecular:
	 *            peso molecular corresponde a un compuesto buscado
	 * @return vectorDeCompuestos[indice].getNombre(): nombre del compuesto que
	 *         tiene el peso molecular ingresado
	 */
	public String busquedaPorPesoMolecularCompuesto(double pesoMolecular) {
		if (vectorDeCompuestos != null) {
			for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
				if (vectorDeCompuestos[indice].getPesoMolecular() == pesoMolecular) {
					return vectorDeCompuestos[indice].getNombre();
				}
			}
		}
		return "Ning�n compuesto cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param densidad:
	 *            variable que corresponde a un compuesto
	 * @return vectorDeCompuestos[indice].getNombre(): nombre que corresponde a
	 *         la densidad ingresada
	 */
	public String busquedaPorDensidadCompuesto(double densidad) {
		if (vectorDeCompuestos != null) {
			for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
				if (vectorDeCompuestos[indice].getDensidad() == densidad) {
					return vectorDeCompuestos[indice].getNombre();
				}
			}
		}
		return "Ning�n compuesto cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param indiceDeRefraccion:
	 *            n�mero que se desea buscar entre los elementos
	 * @return vectorDeCompuestos[indice].getNombre(): nombre del compuesto que
	 *         corresponde al �ndice ingresado
	 */
	public String busquedaPorIndiceDeRefraccionCompuesto(double indiceDeRefraccion) {
		if (vectorDeCompuestos != null) {
			for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
				if (vectorDeCompuestos[indice].getIndiceDeRefraccion() == indiceDeRefraccion) {
					return vectorDeCompuestos[indice].getNombre();
				}
			}
		}
		return "Ning�n compuesto cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param toxicidad:
	 *            valor que se desea buscar entre las caracter�sticas de un
	 *            compuesto
	 * @return vectorDeCompuestos[indice].getNombre(): nombre del compuesto con
	 *         las caracter�sticas ingresadas
	 */
	public String busquedaPorToxicidadCompuesto(String toxicidad) {
		if (vectorDeCompuestos != null) {
			for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
				if (vectorDeCompuestos[indice].getToxicidad().equalsIgnoreCase(toxicidad)) {
					return vectorDeCompuestos[indice].getNombre();
				}
			}
		}
		return "Ning�n compuesto cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param puntoDeEbullicion:
	 *            valor que se desea buscar entre las caracter�sticas de los
	 *            compuestos
	 * @return vectorDeCompuestos[indice].getNombre(): nombre que corresponde al
	 *         compuesto con el punto de ebullici�n ingresado
	 */
	public String busquedaPorPuntoDeEbullicionCompuesto(double puntoDeEbullicion) {
		if (vectorDeCompuestos != null) {
			for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
				if (vectorDeCompuestos[indice].getPuntoDeEbullicion() == puntoDeEbullicion) {
					return vectorDeCompuestos[indice].getNombre();
				}
			}
		}
		return "Ning�n compuesto cumpli� con la caracter�stica ingresada";
	}

	/**
	 * M�todo que realiza una b�squeda de una caracter�stica espec�fica Req:
	 * Variables inicializadas Efect: La b�squeda de una caracter�stica en
	 * espec�fico Mod: N/A
	 * 
	 * @param puntoDeFusion:
	 *            valor que se desea encontrar entre las caracter�sticas de los
	 *            compuestos
	 * @return vectorDeElementos[indice].getNombre(): nombre del compuesto que
	 *         posee la caracter�stica ingresada
	 */
	public String busquedaPorPuntoDeFusionCompuesto(double puntoDeFusion) {
		if (vectorDeCompuestos != null) {
			for (int indice = 0; indice < vectorDeCompuestos.length; indice++) {
				if (vectorDeCompuestos[indice].getPuntoDeFusion() == puntoDeFusion) {
					return vectorDeCompuestos[indice].getNombre();
				}
			}
		}
		return "Ning�n compuesto cumpli� con la caracter�stica ingresada";
	}

	/**
	 * Bibliograf�a
	 * http://www.sc.ehu.es/sbweb/fisica/cursoJava/fundamentos/archivos/objetos.htm
	 * 
	 */

}
